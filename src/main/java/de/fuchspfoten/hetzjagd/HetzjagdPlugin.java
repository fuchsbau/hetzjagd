package de.fuchspfoten.hetzjagd;

import de.fuchspfoten.hetzjagd.modules.AttackSpeedModule;
import de.fuchspfoten.hetzjagd.modules.AttributeModifierModule;
import de.fuchspfoten.hetzjagd.modules.ProjectileKnockbackModule;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main plugin class.
 */
public class HetzjagdPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static HetzjagdPlugin self;

    @Override
    public void onEnable() {
        self = this;

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Register command executor modules.
        // ...

        // Register modules.
        getServer().getPluginManager().registerEvents(new AttackSpeedModule(), this);
        getServer().getPluginManager().registerEvents(new AttributeModifierModule(), this);
        getServer().getPluginManager().registerEvents(new ProjectileKnockbackModule(), this);
    }
}
