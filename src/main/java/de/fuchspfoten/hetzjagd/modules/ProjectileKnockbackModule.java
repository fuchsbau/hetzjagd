package de.fuchspfoten.hetzjagd.modules;

import org.bukkit.Bukkit;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;

/**
 * Gives knockback to eggs and snowballs.
 */
public class ProjectileKnockbackModule implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onProjectileHit(final ProjectileHitEvent event) {
        if (event.getEntity() instanceof Snowball || event.getEntity() instanceof Egg) {
            if (event.getHitEntity() instanceof Player) {
                //noinspection deprecation
                final EntityDamageByEntityEvent damageEvent = new EntityDamageByEntityEvent(event.getEntity(),
                        event.getHitEntity(), DamageCause.PROJECTILE, 0.001);
                Bukkit.getPluginManager().callEvent(damageEvent);
                if (damageEvent.isCancelled()) {
                    return;
                }

                final Damageable hitEntity = (Damageable) event.getHitEntity();
                if (event.getEntity().getShooter() instanceof Entity) {
                    hitEntity.damage(0.001, (Entity) event.getEntity().getShooter());
                } else {
                    hitEntity.damage(0.001);
                }
            }
        }
    }
}
