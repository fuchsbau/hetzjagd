package de.fuchspfoten.hetzjagd.modules;

import de.fuchspfoten.hetzjagd.Unsafe;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

/**
 * A module that modifies attack speed attributes of items.
 */
public class AttributeModifierModule implements Listener {

    /**
     * Updates the given item stack with respect to attributes.
     *
     * @param item The item.
     * @return The possibly modified item.
     */
    private static ItemStack updateItem(final ItemStack item) {
        if (item == null) {
            return null;
        }

        // Do not touch items with lore.
        if (item.hasItemMeta() && item.getItemMeta().hasLore()) {
            return item;
        }

        // Modify item attack values.
        switch (item.getType()) {
            case WOOD_AXE:
                return Unsafe.cloneWithAttackDamage(item, 3.0);
            case STONE_AXE:
                return Unsafe.cloneWithAttackDamage(item, 4.0);
            case IRON_AXE:
                return Unsafe.cloneWithAttackDamage(item, 5.0);
            case GOLD_AXE:
                return Unsafe.cloneWithAttackDamage(item, 3.0);
            case DIAMOND_AXE:
                return Unsafe.cloneWithAttackDamage(item, 6.0);
            case WOOD_PICKAXE:
                return Unsafe.cloneWithAttackDamage(item, 2.0);
            case STONE_PICKAXE:
                return Unsafe.cloneWithAttackDamage(item, 3.0);
            case IRON_PICKAXE:
                return Unsafe.cloneWithAttackDamage(item, 4.0);
            case GOLD_PICKAXE:
                return Unsafe.cloneWithAttackDamage(item, 2.0);
            case DIAMOND_PICKAXE:
                return Unsafe.cloneWithAttackDamage(item, 5.0);
            case WOOD_SPADE:
                return Unsafe.cloneWithAttackDamage(item, 1.0);
            case STONE_SPADE:
                return Unsafe.cloneWithAttackDamage(item, 2.0);
            case IRON_SPADE:
                return Unsafe.cloneWithAttackDamage(item, 3.0);
            case GOLD_SPADE:
                return Unsafe.cloneWithAttackDamage(item, 1.0);
            case DIAMOND_SPADE:
                return Unsafe.cloneWithAttackDamage(item, 4.0);
            case WOOD_HOE:
            case STONE_HOE:
            case IRON_HOE:
            case GOLD_HOE:
            case DIAMOND_HOE:
                return Unsafe.cloneWithAttackDamage(item, 0.0);
            default:
                break;
        }
        return item;
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        for (int i = 0; i < event.getPlayer().getInventory().getContents().length; i++) {
            event.getPlayer().getInventory().setItem(i, updateItem(event.getPlayer().getInventory().getItem(i)));
        }
        event.getPlayer().updateInventory();
    }

    @EventHandler
    public void onCraftItem(final CraftItemEvent event) {
        event.setCurrentItem(updateItem(event.getCurrentItem()));
        if (event.getWhoClicked() instanceof Player) {
            ((Player) event.getWhoClicked()).updateInventory();
        }
    }
}
