package de.fuchspfoten.hetzjagd;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagDouble;
import net.minecraft.server.v1_12_R1.NBTTagInt;
import net.minecraft.server.v1_12_R1.NBTTagList;
import net.minecraft.server.v1_12_R1.NBTTagString;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

/**
 * Unsafe operation helper.
 */
public final class Unsafe {

    /**
     * Clones the given item and changes the attack damage.
     *
     * @param item The item.
     * @param damage The new attack damage.
     * @return The cloned item.
     */
    public static ItemStack cloneWithAttackDamage(final ItemStack item, final double damage) {
        final net.minecraft.server.v1_12_R1.ItemStack itemHandle = CraftItemStack.asNMSCopy(item);

        // Fetch the "Tag".
        final NBTTagCompound itemTag;
        if (itemHandle.hasTag()) {
            itemTag = itemHandle.getTag();
            assert itemTag != null;
        } else {
            itemTag = new NBTTagCompound();
        }

        // Get the attribute modifiers.
        final NBTTagList modifiers = new NBTTagList();

        // Create the attribute modifier for attack damage.
        final NBTTagCompound damageAttribute = new NBTTagCompound();
        damageAttribute.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damageAttribute.set("Name", new NBTTagString("generic.attackDamage"));
        damageAttribute.set("Amount", new NBTTagDouble(damage));
        damageAttribute.set("Operation", new NBTTagInt(0));
        damageAttribute.set("UUIDLeast", new NBTTagInt(894654));
        damageAttribute.set("UUIDMost", new NBTTagInt(2872));
        damageAttribute.set("Slot", new NBTTagString("mainhand"));
        damageAttribute.set("Slot0", new NBTTagString("offhand"));

        // Add the modifier.
        modifiers.add(damageAttribute);
        itemTag.set("AttributeModifiers", modifiers);
        itemHandle.setTag(itemTag);
        return CraftItemStack.asBukkitCopy(itemHandle);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Unsafe() {
    }
}
